"use strict";
let args="xxx", url=null, dbg=false, argsA=args.split(" ");

if (["dbg", "db", "debug"].includes(argsA[0])) {
	console.log("🔎 Golemio bookmarklet debug mode on.");
	console.log(`👉 Started with arguments: "${args}".`);
	console.log(argsA);
	argsA.shift();
	dbg=true;
}

let cfg=[
{
	"cmd": ["w", "ww", "www", "web", "g", "gol", "gl"],
	"url": "https://golemio.cz/"
},
{
	"cmd": ["bi", "bi2"],
	"url": "https://bi.golemio.cz/",
	"search": "?search="
},
{
	"cmd": ["a", "api", "d", "docs"],
	"url": "https://api.golemio.cz/docs/openapi/",
	"arg": [
	{
		"cmd": ["p", "pid", "transport", "trans", "tran", "tr", "t"],
		"url": "https://api.golemio.cz/pid/docs/openapi/"
	}]
},
{
	"cmd": ["k", "key", "keys"],
	"url": "https://api.golemio.cz/api-keys"
},
{
	"cmd": ["vm", "vymi", "vmi", "vymy", "vimi", "vimy"],
	"url": "https://vymi-alerts.rabin.oictinternal.cz/"
},
{
	"cmd": ["vmr", "vymir", "vymip", "vymipid", "vymid", "vmp", "vmr", "vmrp", "vmo", "vymiold"],
	"url": "https://vymipid.ropid.cz/mimolist.aspx"
},
{
	"cmd": ["mp", "mpid", "mapid", "mapapid", "maparopid", "mapp"],
	"url": "https://mapa.pid.cz/",
	"search": "?filter="
},
{
	"cmd": ["tabla", "tbl", "tb", "t"],
	"url": "https://pid.cz/zastavkova-tabla/",
	"search": "?stop="
},
{
	"cmd": ["src", "source", "code"],
	"url": "https://gitlab.com/met1/golemio-bookmarklets"
},
{
	"cmd": ["b", "boa", "board", "brd", "boards", "dash", "dashboard", "dashboards"],
	"url": "https://gitlab.com/groups/operator-ict/golemio/-/boards/",
	"arg": [
	{
		"cmd": ["v", "vv", "vvj", "vyv", "vyvo", "vyvoj"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/boards/2679333?label_name[]=team%2Fvyvoj",
		"search": "&search="
	},
	{
		"cmd": ["d", "da", "dat", "dt", "data", "datar", "datari"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/boards/2553740?label_name[]=team%2Fdata",
		"search": "&search="
	},
	{
		"cmd": ["vm", "vymi"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/boards/5930062?label_name[]=project%3A%3AJIS%3A%20VYMI%20Alerts",
		"search": "&search="
	},
	{
		"cmd": ["j", "ji", "jis"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/boards/7069123?label_name[]=JIS",
		"search": "&search="
	}]
},
{
	"cmd": ["i", "is", "iss", "issue", "issues"],
	"url": "https://gitlab.com/groups/operator-ict/golemio/-/issues/",
	"arg": [
	{
		"cmd": ["t", "todo", "td"],
		"url": "https://gitlab.com/dashboard/todos"
	},
	{
		"cmd": ["vm", "vymi"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/jis/vymi-alerts/-/issues",
		"search": "?search="
	},
	{
		"cmd": ["j", "ji", "jis"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/jis/-/issues/",
		"search": "?search="
	},
	{
		"cmd": ["g", "gl", "gol", "glm", "golem", "golemio"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/issues/",
		"search": "?search="
	},
	{
		"cmd": ["r", "re", "ref", "refine", "refinemen", "refinement", "refnment"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/issues/?sort=relative_position&state=opened&weight=None&label_name%5B%5D=team%2Fvyvoj&label_name%5B%5D=state%3A%3ABacklog",
		"search": "&search="
	},
	{
		"cmd": ["v", "vv", "vvj", "vyv", "vyvo", "vyvoj"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/issues/?label_name%5B%5D=team%2Fvyvoj",
		"search": "&search="
	},
	{
		"cmd": ["d", "da", "dat", "dt", "data", "datar", "datari"],
		"url": "https://gitlab.com/groups/operator-ict/golemio/-/issues/?label_name%5B%5D=team%2Fdata",
		"search": "&search="
	}]
},
{
	"cmd": ["h", "help", "hlp", "pomoc"],
	"code": cmdHelp
}];


function cmdHelp(argsA) {
	let s="🆘🆘 Help/documentation: 🆘🆘\n\n";

	cfg.forEach((item) => {s+="👉 "+JSON.stringify(item.cmd)+"\n"});
	alert(s);
}

function handleConfigItem(item, argsA) {

	if (argsA.length == 0 || item.length == 2) {
		if (item.url) url = item.url;
		if (item.code) item.code();
	}
	else if (item.search) {
		let search = argsA.join(" ");
		url = `${item.url}${item.search}${search}`;
		if (dbg) console.log(`❔ Prepare query: ${search}`);
	}
	else if (item.arg) {

		for (const arg of item.arg) {
			if (arg.cmd.includes(argsA[0])) {
				if (dbg) console.log(`☑️ Matched arg "${argsA[0]}".`);
				handleConfigItem(arg, argsA.slice(1));
				if (dbg) console.log(`✍️ Composed url: ${url}`);
			}
		}
	}
}

let cmd=argsA.shift();

for (const cfgItem of cfg) {

	if (cfgItem.cmd.includes(cmd)) {
		if (dbg) console.log(`✅ Matched command "${cmd}".`);
		handleConfigItem(cfgItem, argsA);
		if (dbg) console.log(`📄 Open page: ${url}`);
	}
}

if (dbg) debugger;
if (url) window.location.href = url;