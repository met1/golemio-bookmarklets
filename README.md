# golemio-bookmarklets

## Getting started

1. Use [bookmarklet maker](https://caiorss.github.io/bookmarklet-maker/) to generate bookmarklet from this file
2. Do not forget to change "xxx" to "%s" in generated bookmarlet code
3. Add bookmarklet as search engine to chrome, see [description](https://zapier.com/blog/add-search-engine-to-chrome/)

